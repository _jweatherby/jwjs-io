const bows = require('bows');

const LOGGER = bows('actions')

const addConnection = (channels, payload) => {
    let reqUrl = url.parse(payload.url),
        sessionKey = payload.headers[socketKey],
        channelId = reqUrl.path.substring(1),
        channelSockets = channels[channelId] || [];
    channelSockets.push(payload)
    return Object.assign({}, channels, {[channelId]: channelSockets})
}

const removeConnection = (channels, payload) => {
    LOGGER(payload)
}

module.exports = {
    addConnection
}
