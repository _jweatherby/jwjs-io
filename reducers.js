const {combineReducers} = require('redux');
const {
    addConnection
} = require('./actions');

const socketReducer = (state = {}, action) => {
    switch(action.type){
        case 'ADD_CONNECTION':
            return addConnection(state, action);
        case 'REMOVE_CONNECTION':
            return removeConnection(state, action);
        default:
            return state;
    }
}

exports.default = combineReducers({channels: socketReducer});
