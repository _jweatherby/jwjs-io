const os = require('os');
const Url = require('url');
const LOGGER = require('bows')('channel');

const SOCKET_KEY = 'sec-websocket-key';

class ChannelMan {

    constructor(){
        this.channels = {};
    }

    hasChannel(channelId){
        return !!this.channels[channelId];
    }

    addChannel(channel){
        this.channels[channel.id] = channel;
    }

    rmChannel(channelId){
        delete this.channels[channelId];
    }

    addSocket(channel, socketMeta, socket, pingInterval){
        if(!this.channels[channel.id]) {
            this.channels[channel.id] = channel
        }
        return this.channels[channel.id]
                .addSocket(socketMeta, socket, pingInterval);
    }

    removeSocket(channel, socketMeta){
        if(channel.rmSocket) {
            channel.rmSocket(socketMeta.headers[SOCKET_KEY]);
        }
    }

    send(channelId, payload){
        if(this.channels[channelId]) {
            this.channels[channelId].send(payload);
        }
    }
}

class Channel {

    constructor(identifier){
        // this is a new connection, will always need to start here
        this.id = identifier;
        this.host = 'some machine id';
        this.sockets = {};
    }

    channelKey(id){
        return `channel-${id || this.id}`;
    }

    addSocket(socketMeta, ws, pingInterval){
        let socket = new Socket(socketMeta, ws);
        socket.startPinging(pingInterval)
        this.sockets[socket.id] = socket;
        return this.sockets[socket.id]
    }

    rmSocket(id){
        delete this.sockets[id];
        LOGGER("socket removed", id);
        this.socketCount();
    }

    socketCount(){
        let numSockets = Object.keys(this.sockets).length;
        LOGGER("Channel Sockets:", numSockets,
            "ChannelID:", this.id
        )
        return numSockets
    }

    getChannelId(reqUrl){
        // the id is the string attached to the end of the url
        // this will identify the channel and in redis
        // FIXME: this should probably change as that's a lot
        // of exposure.
        let url = Url.parse(reqUrl);
        return url.path.substring(1);
    }

    send(msg){
        for(var key in this.sockets){
            try {
                this.sockets[key].send(msg);
            } catch(err){
                LOGGER("Removing dead socket", err.message);
                this.rmSocket(key);
            }
        }
    }
}

class Socket {

    constructor(socketInfo, socket){
        this.id = this.getSessionKey(socketInfo)
        this.socket = socket;
        this.isAlive = true;
    }

    getSessionKey(meta){
        return meta.headers[SOCKET_KEY];
    }

    send(payload){
        this.socket.send(JSON.stringify(payload));
    }

    startPinging(pingInterval){
        setInterval(() => {

            if(!this.isAlive) this.socket.terminate()

            this.isAlive = false
            this.socket.ping('', false, true)

        }, (pingInterval || 30) * 1000)
    }
}

module.exports = {Channel, ChannelMan}
