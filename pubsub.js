const LOGGER = require('bows')('pubsub');
const redisCli = require('redis');
const {takeEvery, eventChannel} = require('redux-saga');
const {select, fork, call, put, take} = require('redux-saga/effects');
const getRedis = require('./redis')

function* subscribe(sub, channelName){
    return eventChannel(emit => {
        sub.subscribe(channelName);
        sub.on('subscribe', function(channel){
            LOGGER(`Subscribed to ${channel}!`)
        })

        sub.on('message', (channel, msg) => {
            let action = JSON.parse(msg),
                {toRelay, req} = action;
            emit({req, toRelay});
        })

        return function(e){ }
    })
}

function* listen(eventChannel){
    while(true){
        let event = yield take(eventChannel);
        let {req, toRelay} = event;
        yield put({type: 'RELAY', req, toRelay})
    }
}

function* publish(cli, channelName){
    while(true){
        let action = yield take('PUBSUB.PUBLISH');
        cli.publish(channelName, JSON.stringify(action));
    }
}

function* pubsubSaga(redisCli, channelName){

    if(!redisCli) {
        // exit early defaulting to non-redis strategy
        LOGGER("Using non-redis option")
        return yield takeEvery('PUBSUB.PUBLISH', function* ({toRelay, req}){
            yield put({type: 'RELAY', req, toRelay});
        });
    }
    let pub = redisCli.duplicate(),
        sub = redisCli.duplicate();

    let channel = yield call(subscribe, sub, channelName);

    yield [
        fork(function*(){ yield call(listen, channel) }),
        fork(function*(){ yield call(publish, pub, channelName) }),
    ]
}

exports.default = pubsubSaga;
