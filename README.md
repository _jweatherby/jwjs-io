JWJS-IO
=======

An engine to route socket requests and manage the channels.


Installation
------------

    yarn add jwjs-io


Add the following snippet:

    const {jwjsIO} = require('jwjs-io');
    jwjsIO(wss, routeRequests);


What are routeRequests?
-----------------------

It's a switch statement, or however you'd like to implement it, routing requests
by what sort of action you'd like to execute. Taking a cue from redux, it reduces
the request into a digestible view, with whatever backend you'd like behind it.

    cosnt routeRequests = (req) => {
        switch(req.type){
            case 'DO_SOME_ACTION':
                return doSomeAction(req);
            default:
                return req;
        }
    }

The default is to simply relay the payload to all the selected members. See #Recipients


Actions
-------

They can be as simple as adding an id to an object

    function someAction(req){
        req.id = (new Date).now()
        return {...req}
    }

You can use generators to fulfill these requests. There's a redux store in the back,
so you can use redux and redux-saga to dispatch further actions.

    function* doSomeAction(req){
        yield call(doAnotherAction, param)
    }

A special trigger called `TRIGGER_MSG` can asynchronously do something else while the
first message is processing.

    function* doSomeAction(req){
        const payload = req.someObj;
        const newReq = Object.assign(req, {type: 'NEW_ACTION', payload})

        yield put({type: 'TRIGGER_MSG', req: newReq})
        return req
    }


Recipients
----------

A list of ids that will also be associated with their channel.

    {
        'sender': 'someidhere',
        'recipients': ['id1', 'id2', ...],
    }

You can pass in a function when initializing to request this id (making it more secure).

    jwjsIO(wss, routeRequests, {
        getConnection: function(socketToken){
            API.post('https://api.somedomain.com/getId').then(data => data.userId);
        }
    })

Passing the id will use that user id as the token used to associate messages.

If nothing is passed, the default is to use the url string.

    wss://somedomain.com/<user_identifier>


Multi-server
------------

You can pass in redis credentials (host, port)

    jwjsIO(wss, routeRequests, {
         host: 'localhost',
         port: 6379,
         channelName: 'jwjs-io'
    })
