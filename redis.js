const LOGGER = require('bows')('redisCli');
const redis = require('redis');
const bluebird = require('bluebird')

let redisCli;

bluebird.promisifyAll(redis.RedisClient.prototype)
bluebird.promisifyAll(redis.Multi.prototype);

let configureRedis = (redisOpts) => {

    LOGGER("Initializing redis", redisOpts)

    let redisConfig = {};
    let {host, port, channelName, prefix} = redisOpts;

    if(host) redisConfig.host = host;
    if(port) redisConfig.port = port;
    if(prefix) redisConfig.prefix = prefix;

    return redisConfig
}

exports.default = (redisOpts=null) => {
    if(!redisCli && redisOpts){
        let redisConfig = configureRedis(redisOpts)
        redisCli = redis.createClient(redisConfig)
    } else if(!redisCli && !redisOpts){
        throw 'Redis not initialized in getRedis'
    }
    LOGGER('Initialized redisCli')
    return redisCli
}
