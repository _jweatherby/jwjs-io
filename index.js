/**
 * We need redux and especially redux-saga to handle the actions.
 * The sagas should be able to handle fetching additional data
 * when evaluating the data.
 */
const {compose, applyMiddleware, createStore} = require('redux'),
    createSagaMiddleware = require('redux-saga').default,
    {put, fork} = require('redux-saga/effects'),
    LOGGER = require('bows')('index');

const jwjsIOReducer = require('./reducers').default,
    jwjsIOSaga = require('./sagas').default,
    pubsubSaga = require('./pubsub').default;

const initRedis = require('./redis').default

let redisCli;

function jwjsIO(wss, routeRequest, opts={}){

    // run that shite
    if(opts.RedisOpts){
        redisCli = initRedis(opts.RedisOpts)
    }
    sagaMiddleware = createSagaMiddleware();
    createStore(jwjsIOReducer, applyMiddleware(sagaMiddleware));

    sagaMiddleware.run(function*(){
        yield [
            fork(function*(){ yield jwjsIOSaga(wss, routeRequest, opts)}),
            fork(function*(){ yield pubsubSaga(redisCli, opts.RedisOpts.channelName || 'jwjs-io')}),
        ]
    });
}

function *jwjsTrigger(req){
    yield put({type: 'FORK', req})
}

module.exports = {
    jwjsIO, jwjsIOReducer, jwjsIOSaga, jwjsTrigger
}
