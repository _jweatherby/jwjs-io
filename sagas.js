const LOGGER = require('bows')('sagas');
const http = require('http').default;
const {fork, call, put, take} = require('redux-saga/effects');
const {takeEvery, eventChannel} = require('redux-saga');
const axios = require('axios').default;

const {Channel, ChannelMan} = require('./channel');
const getRedis = require('./redis').default

// this will manage the channels and dispatch messages
// as appropriate.
let channelMan = new ChannelMan();

// make the route requests (views) global in this scope
// (initialized in initSocket)
let ROUTE_REQUEST;

let firebaseAxios;

function* subscribe(ws, {getChannel, pingInterval}){
    return eventChannel(emit => {
        ws.on('connection', function(connection, reqInfo){
            ws.isAlive = true

            // if connect is not a function where we can extract the id
            // from some other place, use the url provided.
            if(typeof getChannel != 'function'){
                getChannel = () => new Promise(() => {
                    setTimeout(() => {
                        return reqInfo.url.slice(1)
                    }, 0)
                })
            }

            getChannel(reqInfo).then(identifier => {

                let channel = new Channel(identifier);

                // Add the ws to the channel manager
                let socket = channelMan.addSocket(
                    channel, reqInfo, connection, pingInterval
                );

                LOGGER("Hello Connection", channel.id);

                connection.on('pong', () => {
                    socket.isAlive = true;
                })

                connection.on('message', data => {
                    emit(JSON.parse(data))
                });

                connection.on('close', function(e){
                    channelMan.removeSocket(channel, reqInfo)
                })
            })
        })

        // unsubscribe function
        return function*(e){
            yield put({type: 'REMOVE_CONNECTION', e})
        };
    });
}

function* listen(eventChannel){
    while(true){
        let req = yield take(eventChannel);
        if(req.type == 'SOCKET.SET_FIREBASE_TOKEN'){
            yield call(setFirebaseToken, req.payload)
        } else {
            yield call(evaluateReq, req);
        }
    }
}

function* setFirebaseToken({firebaseToken, accountId}){
    try {
        const redisCli = getRedis()
        let key = `firebase_token:${accountId}`
        let tokens = yield redisCli.lrangeAsync(key, 0, -1)
        if((tokens || []).indexOf(firebaseToken) == -1){
            LOGGER('pushing firebase token')
            redisCli.lpush(key, firebaseToken)
        }
    } catch(err) {
        LOGGER('setFirebaseToken error', err)
    }
}

// Triggers the response
function* evaluateReq(req){
    let {sender, recipients, notToSender} = req;
    let {payload, type} = yield call(ROUTE_REQUEST, req);
    if(!type) { type = req.type; }

    yield put({
        type: 'PUBSUB.PUBLISH',
        req: {sender, recipients, notToSender},
        toRelay: {payload, type}
    })

    if(req.firebaseMsg){
        for(let userId of recipients){
            if(userId != sender){
                yield call(sendFirebaseMsg, {
                    userId,
                    firebaseMsg: req.firebaseMsg
                })
            }
        }
    }
}

function* relayMsg({req, toRelay}){
    if(!req.notToSender){
        // channels needs to be an object with send
        // or `sendOverChannel()`
        channelMan.send(req.sender, toRelay);
    }

    if(!(req.recipients && req.recipients.length)){
        // #FIXME: this should be an error, maybe?
        // LOGGER('Sending this anyway, but there\'s no recipients listed')
    }

    // then go through and distribute
    (req.recipients || []).map(userId => {
        if(channelMan.hasChannel(userId) && userId != req.sender) {
            channelMan.send(userId, toRelay);
        }
    });
}

function* sendFirebaseMsg({userId, firebaseMsg}){
    try {
        const redisCli = getRedis();
        let key = `firebase_token:${userId}`
        let tokens = yield redisCli.lrangeAsync(key, 0, -1)
        for(let token of tokens){
            let resp = yield firebaseAxios.post(
                'https://fcm.googleapis.com/fcm/send',
                {to: token, data: firebaseMsg}
            )
            if(resp.data.failure) {
                LOGGER('Removing from redis', token)
                yield redisCli.lremAsync(key, 1, token)
            }

        }
    } catch(err){
        LOGGER('firebaseMsg err', err.message, err.reason)
    }
}

function* destroySocket(socket){
    socket.destroy();
}

function* triggerMsg(req){
    while(true){
        let action = yield take('FORK');
        let {req} = action;
        yield call(evaluateReq, req);
    }
}

function* runSocket(eventChannel){
    yield [
        fork(function*(){ yield call(listen, eventChannel) }),
        fork(function*(){ yield call(triggerMsg) }),
        fork(function*(){ yield takeEvery('RELAY', relayMsg) }),
    ]
}

function* initSocket(socket, routeRequest, opts){
    // this is dirty, but whatevs
    ROUTE_REQUEST = routeRequest;
    let eventChannel = yield call(subscribe, socket, opts)
    if(opts.FIREBASE_API_KEY){
        firebaseAxios = axios.create({
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `key=${opts.FIREBASE_API_KEY}`
            }
        })
    }
    yield call(runSocket, eventChannel)
}

exports.default = initSocket
